import { Component, OnInit, ɵConsole } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';

@Component({
  selector: 'app-gestionar-acta-recurso',
  templateUrl: './gestionar-acta-recurso.component.html',
  styleUrls: ['./gestionar-acta-recurso.component.css'],
  providers: [UserService]
})
export class GestionarActaRecursoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  public status: string = "delete";
  public perfil: string;
  public p;
  public ruta;
  public action;

  constructor(
    public _userService: UserService
  ) { 
    this.perfil = this._userService.getPerfil();
    this.p = global.perfiles;
    this.ruta = global.ruta;
  }

  posts = [
    {
      "id": 170800186,
      "rbd": "4791",
      "cantidad": "$ 9.877.105",
      "date": "04/11/2017"
    }
  ];
  
  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");
    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      }
    };
  }

  onSubmit(form){
    this.status = "success";
    console.log(this.status);
  };
  EliminaRegistro(forma){
    this.status = "delete";
    console.log(this.status);
    
  }

}
