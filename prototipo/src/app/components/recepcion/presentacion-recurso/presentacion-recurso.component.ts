import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';

@Component({
  selector: 'app-presentacion-recurso',
  templateUrl: './presentacion-recurso.component.html',
  styleUrls: ['./presentacion-recurso.component.css'],
  providers: [UserService]
})
export class PresentacionRecursoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  public argumento;
  public perfil: string;
  public p;
  public ruta;

  constructor(
    public _userService: UserService
  ) {
    this.perfil = this._userService.getPerfil();
    this.argumento = localStorage.getItem('argumento');
    this.p = global.perfiles;
    this.ruta = global.ruta;
  }

  

  posts = [
    {
      "id": 123,
      "seguimiento": "3463-2",
      "RBD": "",
      "noAceptado": "",
      "monto": "",
      "fecha": ""
    },
    {
      "id": 127,
      "seguimiento": "3456-7",
      "RBD": "",
      "noAceptado": "",
      "monto": "",
      "fecha": ""
    },
    {
      "id": 132,
      "seguimiento": "Sostenedor",
      "RBD": "",
      "noAceptado": "",
      "monto": "",
      "fecha": ""
    }

  ];

  postsRemuneracion = [
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "cuenta": "Sueldo base",
      "n_cuenta": "410101",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 392.416",
      "montoimpugnado": "$ 392.416"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "cuenta": "Incremento % zona",
      "n_cuenta": "410104",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 78.483",
      "montoimpugnado": "$ 78.483"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "cuenta": "Seguro de accidente del trabajo",
      "n_cuenta": "410401",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 4.474",
      "montoimpugnado": "$ 4.474"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "cuenta": "Seguro de cesantía",
      "n_cuenta": "410402",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 14.127",
      "montoimpugnado": "$ 14.127"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "cuenta": "Seguro de invalidez y sobrevivencia",
      "n_cuenta": "410403",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 5.415",
      "montoimpugnado": "$ 5.415"
    }


  ];

  postsBoletas = [
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "n_doc": "51681227",
      "fecha_doc": "12/01/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 20.000",
      "montoimpugnado": "$ 20.000"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "n_doc": "42467002",
      "fecha_doc": "18/08/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 25.000",
      "montoimpugnado": "$ 25.000"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "n_doc": "42842193",
      "fecha_doc": "14/09/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "n_doc": "42411277",
      "fecha_doc": "18/08/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "n_doc": "49185031",
      "fecha_doc": "04/11/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    }


  ];
  ngOnInit() {
    
    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      }
    };
  }

}
